/// <reference types="./add-dom-event-listener" />
/// <reference types="./is-absolute-url" />
/// <reference types="./same-origin" />

import { JSDOM } from "jsdom";
import * as tape from "tape";
import url = require("url");
import { createLocation } from ".";

const createJSDOM = (body: string = "") =>
  new JSDOM(`<!DOCTYPE html><body>${body}</body>`, {
    url: "https://example.org/",
    referrer: "https://example.com/"
  });

const OPTIONS = { html5Mode: false };

tape("location.init", (assert: tape.Test) => {
  const location = createLocation(createJSDOM().window);

  location.init();
  location.init();

  assert.equals(location.get().pathname, "/");
  location.remove();

  assert.end();
});

tape("location setOnly setHandler setHtml5Mode", (assert: tape.Test) => {
  const location = createLocation(createJSDOM().window);

  location.init();

  location.setOnly("/pathname");
  assert.equals(location.get().pathname, "/pathname");

  location.setHandler(prevHandler => (url: url.UrlWithParsedQuery) =>
    prevHandler(url)
  );
  location.setHtml5Mode(false);

  location.remove();

  assert.end();
});

tape("location history", (assert: tape.Test) => {
  const jsdom = createJSDOM();
  const location = createLocation(jsdom.window, OPTIONS);

  location.init().then(() => {
    location.set("/pathname").then(() => {
      location.back();
      jsdom.window.dispatchEvent(new jsdom.window.Event("hashchange"));
      setTimeout(() => {
        assert.equals(location.get().pathname, "/");

        location.forward();
        jsdom.window.dispatchEvent(new jsdom.window.Event("hashchange"));
        setTimeout(() => {
          assert.equals(location.get().pathname, "/pathname");
          assert.equals(location.history(), 3);
          assert.end();
        }, 10);
      }, 10);
    });
  });
});

tape("location can set the pathname", (assert: tape.Test) => {
  const location = createLocation(createJSDOM().window);

  location.set("/pathname/a/b/c").then(() => {
    assert.equals(
      location.get().pathname,
      "/pathname/a/b/c",
      "location.set('/pathname/a/b/c') sets the path to '/pathname/a/b/c'"
    );
    location.remove();
    assert.end();
  });
});

tape(
  "location set when returned an error does not change the pathname",
  (assert: tape.Test) => {
    const handler = (url: url.UrlWithParsedQuery) =>
      Promise.reject(new Error(url.pathname));

    const location = createLocation(createJSDOM().window, { handler });

    location
      .set("/pathname")
      .catch(e =>
        assert.equals(
          !!e,
          true,
          "a returned rejected promise causes and error to exist."
        )
      )
      .then(() => {
        assert.equals(
          location.get().pathname,
          "/",
          "the path is not changed when a rejected promise is returned"
        );
        location.remove();
        assert.end();
      });
  }
);
