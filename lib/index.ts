export {
  IHandler,
  ISetHandler,
  IOptions,
  urlEqual,
  getUrlPathname,
  Location,
  createLocation
} from "./Location";
