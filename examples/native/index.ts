import addEventListener = require("add-dom-event-listener");
import { createOnLoad } from "../createOnLoad";

addEventListener(
  window,
  "load",
  createOnLoad({
    html5Mode: true
  })
);

if ((module as any).hot) {
  (module as any).hot.accept(() => {
    window.location.reload();
  });
}
