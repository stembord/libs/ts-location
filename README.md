# ts-location

url location handling for browsers

```typescript
import url = require("url");
import { createLocation } from "@stembord/location";

const { parse } = url;

const handler = (url: url.UrlWithParsedQuery) =>
    url.pathname === "/not_allowed"
        ? // redirect to home, rejecting with null will ignore the request
          Promise.reject(parse("/", true))
        : Promise.resolve(url);

const location = createLocation(window, { html5Mode: true, handler });

// succuss
location.set("/allowed").then(() => {
    console.log("succuss");
});

// failure
location.set("/not_allowed").catch(error => {
    console.error(error);
});

// remove DOM listeners
location.remove();
```
